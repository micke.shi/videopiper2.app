import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
    {
        path: '/',
        component: () => import('layouts/MainLayout.vue'),
        children: [
            { path: '', component: () => import('pages/Index.vue') },
            {
                path: 'downloadVideo',
                component: () => import('pages/downloadVideo.vue'),
                props: route => {
                    return {
                        URL: route.query.URL ?? null
                    };
                }
            },
            {
                path: 'queue',
                component: () => import('pages/queue.vue')
            },
            {
                path: 'queueItem/:Id',
                component: () => import('pages/queue.vue')
            }
        ]
    },

    // Always leave this as last one,
    // but you can also remove it
    {
        path: '*',
        component: () => import('pages/Error404.vue')
    }
];

export default routes;
